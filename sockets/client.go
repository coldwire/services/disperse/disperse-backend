package sockets

import (
	"fmt"
	"log"

	"github.com/gofiber/websocket/v2"
)

type Client struct {
	Id      string
	Conn    *websocket.Conn
	Channel *Channel
}

type Message struct {
	Type int    `json:"type"`
	Body string `json:"body"`
}

func (c *Client) Read() {
	defer func() {
		c.Channel.Unregister <- c
		c.Conn.Close()
	}()

	for {
		messageType, p, err := c.Conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		message := Message{Type: messageType, Body: string(p)}
		c.Channel.Broadcast <- message
		fmt.Printf("Message Received: %+v\n", message)
	}
}
