package sockets

import "fmt"

type Channel struct {
	Register   chan *Client
	Unregister chan *Client
	Clients    map[*Client]bool
	Broadcast  chan Message
}

var Channels = make(map[string]*Channel)

func NewChan(id string) *Channel {
	Channels[id] = &Channel{
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		Clients:    make(map[*Client]bool),
		Broadcast:  make(chan Message),
	}

	return Channels[id]
}

func (channel *Channel) Start() {
	for {
		select {
		case client := <-channel.Register:
			channel.Clients[client] = true
			fmt.Println("Size of Connection Pool: ", len(channel.Clients))
			for client, _ := range channel.Clients {
				fmt.Println(client)
				client.Conn.WriteJSON(Message{Type: 1, Body: "New User Joined..."})
			}
			break
		case client := <-channel.Unregister:
			delete(channel.Clients, client)
			fmt.Println("Size of Connection Pool: ", len(channel.Clients))
			for client, _ := range channel.Clients {
				client.Conn.WriteJSON(Message{Type: 1, Body: "User Disconnected..."})
			}
			break
		case message := <-channel.Broadcast:
			fmt.Println("Sending message to all clients in Pool")
			for client, _ := range channel.Clients {
				if err := client.Conn.WriteJSON(message); err != nil {
					fmt.Println(err)
					return
				}
			}
		}
	}
}
