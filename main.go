package main

import (
	"disperse/sockets"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"github.com/google/uuid"
)

func main() {
	app := fiber.New()

	app.Use("/ws", func(c *fiber.Ctx) error {
		if websocket.IsWebSocketUpgrade(c) {
			c.Locals("allowed", true)
			return c.Next()
		}
		return fiber.ErrUpgradeRequired
	})

	app.Get("/chan", fiber.Handler(func(c *fiber.Ctx) error {
		id := uuid.NewString()

		if sockets.Channels[id] != nil {
			return c.JSON(fiber.Map{
				"error": "channel already exist",
			})
		}

		channel := sockets.NewChan(id)
		go channel.Start()

		return c.JSON(fiber.Map{
			"success": id,
		})
	}))

	app.Get("/ws/:channel", websocket.New(func(c *websocket.Conn) {
		chanId := c.Params("channel")

		chn := sockets.Channels[chanId]

		if chn != nil {
			go func() {
				client := &sockets.Client{
					Id:      uuid.NewString(),
					Conn:    c,
					Channel: chn,
				}
				chn.Register <- client

				client.Read()
			}()
		} else {
			c.WriteJSON(fiber.Map{
				"error": "channel '" + chanId + "' does not exist",
			})
		}
	}))

	log.Fatal(app.Listen(":3000"))
}
